package com.dc.IntegrationBOA.IntegrationBOA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class IntegrationBoaApplication {

	public static void main(String[] args) {
		SpringApplication.run(IntegrationBoaApplication.class, args);
	}

}
