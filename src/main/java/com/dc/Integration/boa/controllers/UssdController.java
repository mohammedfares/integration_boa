package com.dc.Integration.boa.controllers;

import com.dc.Integration.boa.exception.ServiceFaultException;
import com.dc.Integration.boa.pojo.MessageBody;
import com.dc.Integration.boa.pojo.UssdRequest;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

@RestController
@RequestMapping("/ussd")
public class UssdController {

    private static final Logger logger = LogManager.getLogger("APP_INTG");

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> startUssd(@RequestBody UssdRequest ussdRequest) {

        if (ussdRequest.getSpId() == null
                || ussdRequest.getSpPassword() == null
                || ussdRequest.getServiceId() == null
                || ussdRequest.getTimeStamp() == null
                || ussdRequest.getEndpoint() == null
                || ussdRequest.getInterfaceName() == null
                || ussdRequest.getCorrelator() == null
                || ussdRequest.getUssdServiceActivationNumber() == null
                || ussdRequest.getCriteria() == null) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Error in request information");
        }

        logger.info("ussdRequest: " + ussdRequest.getServiceId());

        String ussdUrl = environment.getProperty("ussd_url")+"/USSDNotificationManagerService/services/USSDNotificationManager";

        String response = null;
        try {
            String responseString = "";
            String outputString = "";
            String wsURL = ussdUrl;
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/osg/ussd/notification_manager/v1_0/local\">  \n" +
                    "   <soapenv:Header>  \n" +
                    "      <tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com.cn/schema/common/v2_1\">  \n" +
                    "         <tns:spId>"+ussdRequest.getSpId()+"</tns:spId>  \n" +
                    "         <tns:spPassword>"+ussdRequest.getSpPassword()+"</tns:spPassword>  \n" +
                    "         <tns:serviceId>"+ussdRequest.getServiceId()+"</tns:serviceId>  \n" +
                    "         <tns:timeStamp>"+ussdRequest.getTimeStamp()+"</tns:timeStamp>  \n" +
                    "      </tns:RequestSOAPHeader>  \n" +
                    "   </soapenv:Header>  \n" +
                    "   <soapenv:Body>  \n" +
                    "      <loc:startUSSDNotification>  \n" +
                    "         <loc:reference>  \n" +
                    "            <endpoint>"+ussdRequest.getEndpoint()+"</endpoint>  \n" +
                    "            <interfaceName>"+ussdRequest.getInterfaceName()+"</interfaceName>  \n" +
                    "            <correlator>"+ussdRequest.getCorrelator()+"</correlator>  \n" +
                    "         </loc:reference>  \n" +
                    "         <loc:ussdServiceActivationNumber>"+ussdRequest.getUssdServiceActivationNumber()+"</loc:ussdServiceActivationNumber>  \n" +
                    "          <loc:criteria>"+ussdRequest.getCriteria()+"</loc:criteria>  \n" +
                    "      </loc:startUSSDNotification>  \n" +
                    "   </soapenv:Body>  \n" +
                    "</soapenv:Envelope>   \n";


            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "<SOAP action of the webservice to be consumed>";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("ussd start response: " + outputString);

            Document document = stringToDocument(outputString);

            if (document == null) {
                logger.info("document null");
                throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
            }

            if (hasFault(document)) {
                logger.info("hasFault");
                throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
            }

            JSONObject xmlJSONObj = XML.toJSONObject(outputString);
            response = xmlJSONObj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").toString();

        } catch (Exception ex) {
            logger.error(ExceptionUtils.getStackTrace(ex),ex);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
        }

        MessageBody messageBody = MessageBody.getInstance();
        messageBody.setStatus("200");
        messageBody.setText("OK");
        messageBody.setBody(response);

        return new ResponseEntity<>(messageBody, HttpStatus.OK);
    }


    @RequestMapping(value = "/stop", method = RequestMethod.POST)
    public ResponseEntity<MessageBody> stopUssd(@RequestBody UssdRequest ussdRequest) {

        if (ussdRequest.getSpId() == null
                || ussdRequest.getSpPassword() == null
                || ussdRequest.getServiceId() == null
                || ussdRequest.getTimeStamp() == null
                || ussdRequest.getCorrelator() == null) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Error in request information");
        }

        logger.info("ussdRequest: " + ussdRequest.getServiceId());

        String ussdUrl = environment.getProperty("ussd_url")+"/USSDNotificationManagerService/services/USSDNotificationManager";

        String response = null;
        try {
            String responseString = "";
            String outputString = "";
            String wsURL = ussdUrl;
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:loc=\"http://www.csapi.org/schema/osg/ussd/notification_manager/v1_0/local\">  \n" +
                    "   <soapenv:Header>  \n" +
                    "      <tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com.cn/schema/common/v2_1\">  \n" +
                    "         <tns:spId>"+ussdRequest.getSpId()+"</tns:spId>  \n" +
                    "         <tns:spPassword>"+ussdRequest.getSpPassword()+"</tns:spPassword>  \n" +
                    "         <tns:serviceId>"+ussdRequest.getServiceId()+"</tns:serviceId>  \n" +
                    "         <tns:timeStamp>"+ussdRequest.getTimeStamp()+"</tns:timeStamp>  \n" +
                    "      </tns:RequestSOAPHeader>  \n" +
                    "   </soapenv:Header>  \n" +
                    "   <soapenv:Body>  \n" +
                    "      <loc:stopUSSDNotification>  \n" +
                    "         <loc:correlator>"+ussdRequest.getCorrelator()+"</loc:correlator>  \n" +
                    "      </loc:stopUSSDNotification>  \n" +
                    "   </soapenv:Body>  \n" +
                    "</soapenv:Envelope>   \n";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "<SOAP action of the webservice to be consumed>";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("ussd start response: " + outputString);

            Document document = stringToDocument(outputString);

            if (document == null) {
                logger.info("document null");
                throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
            }

            if (hasFault(document)) {
                logger.info("hasFault");
                throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
            }

            JSONObject xmlJSONObj = XML.toJSONObject(outputString);
            response = xmlJSONObj.getJSONObject("soapenv:Envelope").getJSONObject("soapenv:Body").toString();

        } catch (Exception ex) {
            logger.error(ExceptionUtils.getStackTrace(ex),ex);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Error in integration");
        }

        MessageBody messageBody = MessageBody.getInstance();
        messageBody.setStatus("200");
        messageBody.setText("OK");
        messageBody.setBody(response);

        return new ResponseEntity<>(messageBody, HttpStatus.OK);
    }


    private Document stringToDocument(String xmlDocument) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlDocument)));
            return doc;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    private boolean hasFault(Document document) {

        NodeList soapFaultNodeList = document.getElementsByTagName("soap:Fault");

        if (soapFaultNodeList == null || soapFaultNodeList.getLength() == 0) {
            return false;
        } else {
            return true;
        }
    }
}
