package com.dc.Integration.boa.controllers;

import com.dc.Integration.boa.exception.ServiceFaultException;
import com.dc.Integration.boa.pojo.*;
import com.dc.Integration.boa.utils.Utils;
import com.squareup.okhttp.OkHttpClient;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.squareup.okhttp.*;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/dc")
public class DcController {

    private static final Logger logger = LogManager.getLogger("APP_INTG");

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/cashin", method = RequestMethod.POST)
    public CashinRs cashinBoa(HttpServletRequest httpServletRequest, @RequestBody CashinRq cashinRq) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();
        // Set Time Out
        client = Utils.setTimeOut(15, client, TimeUnit.SECONDS);


        JSONObject jsonObject = new JSONObject();
        jsonObject.put("destinationAmount", cashinRq.getAmount());
        jsonObject.put("bankTransactionRef", cashinRq.getReference());
        jsonObject.put("statusCode", cashinRq.getStatus());
        jsonObject.put("destinationReference", cashinRq.getDestinationReference());
        jsonObject.put("description", cashinRq.getDescription());

        logger.info("jsonObject: " + jsonObject.toString());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        String url = environment.getProperty("dw_url") + "/inetgration/boaCashIn";
        Request request = new Request.Builder()
                .url(url)
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        CashinRs cashinRs = new CashinRs();

        if (response != null && response.code() == 200) {
            //sucess Trasnaction
            cashinRs.setStatus("OK");
            cashinRs.setT24Reference(cashinRq.getReference());
            cashinRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else if (response != null && response.code() != 200) {
            // Faild from Core
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        } else {
            // Connection Error
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Integration Error");
        }

        return cashinRs;
    }


    @RequestMapping(value = "/cashout", method = RequestMethod.POST)
    public CashoutRs cashoutBoa(HttpServletRequest httpServletRequest, @RequestBody CashoutRq cashoutRq) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }


        String code = httpServletRequest.getHeader("branchCode");
        OkHttpClient client = new OkHttpClient();
        // Set Time Out
        client = Utils.setTimeOut(15, client, TimeUnit.SECONDS);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("destinationAmount", cashoutRq.getAmount());
        jsonObject.put("bankTransactionRef", cashoutRq.getReference());
        jsonObject.put("statusCode", cashoutRq.getStatus());
        jsonObject.put("sourceReference", cashoutRq.getSourceReference());
//        jsonObject.put("sourceCode", cashoutRq.getSource());
//        jsonObject.put("destinationReference", cashoutRq.getDestinationReference());
        jsonObject.put("description", cashoutRq.getDescription());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/boaCashOut")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", code)
                .addHeader("username", username.toLowerCase())
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        CashoutRs cashoutRs = new CashoutRs();

        if (response != null && response.code() == 200) {
            //sucess
            cashoutRs.setStatus("OK");
            cashoutRs.setT24Reference(cashoutRq.getReference());
            cashoutRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else if (response != null && response.code() != 200) {

            // Faild in Core
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        } else {
            // Connection Error
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Integration Error");
        }

        return cashoutRs;
    }

    @RequestMapping(value = "/billInquiry", method = RequestMethod.GET)
    public InquiryRs billInquiryBoa(HttpServletRequest httpServletRequest,
                                    @RequestParam(value = "billNumber", required = false) String billNumber) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        String code = httpServletRequest.getHeader("code");
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(environment.getProperty("ebill_url") + "/ac/bill?billNumber=" + billNumber)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        InquiryRs inquiryRs = new InquiryRs();

        if (response != null && response.code() == 200) {
            //sucess
            inquiryRs.setStatus("OK");
            inquiryRs.setBillerId(responseJson.getJSONObject("body").getString("billerCode"));
            inquiryRs.setBillerName(responseJson.getJSONObject("body").getString("billerName"));
            inquiryRs.setBillNumber(responseJson.getJSONObject("body").getString("sadadNo"));
            inquiryRs.setAmountDue(String.valueOf(responseJson.getJSONObject("body").getDouble("amountDue")));
            inquiryRs.setServiceName(responseJson.getJSONObject("body").isNull("serviceName") ? "" : responseJson.getJSONObject("body").getString("serviceName"));
            inquiryRs.setBillStatus(responseJson.getJSONObject("body").getString("invoiceStatusCaption"));
        } else if (response != null && response.code() != 200) {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        } else {
            // Connection Error
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Integration Error");
        }

        return inquiryRs;
    }


    @RequestMapping(value = "/billPayment", method = RequestMethod.POST)
    public PaymentRs billIPaymentBoa(HttpServletRequest httpServletRequest,
                                     @RequestBody PaymentRq paymentRq) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("statusCode", paymentRq.getStatus());
        jsonObject.put("bankTransactionRef", paymentRq.getReference());
        jsonObject.put("destinationReference", paymentRq.getBillNumber());
        jsonObject.put("description", paymentRq.getDescription());
//        jsonObject.put("source", paymentRq.getSource());
//        jsonObject.put("sourceReference", paymentRq.getSourceReference());

        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/billPayment")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        PaymentRs paymentRs = new PaymentRs();

        if (response != null && response.code() == 200) {
            //sucess
            paymentRs.setStatus("OK");
            paymentRs.setT24Reference(paymentRq.getReference());
            paymentRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else if (response != null && response.code() != 200) {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        } else {
            // Connection Error
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "Integration Error");
        }

        return paymentRs;
    }

    @RequestMapping(value = "/schoolBillers", method = RequestMethod.GET)
    public MessageRs schoolBillInquiry(HttpServletRequest httpServletRequest) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        String code = httpServletRequest.getHeader("code");
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(30, TimeUnit.SECONDS);
        client.setReadTimeout(30, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/invoice/findAll/SchoolBiller")
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            logger.info("XXX: " + response.body().string());
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        MessageRs messageRs = new MessageRs();

        List<BillerRs> billerRsList = new ArrayList<>();

        if ((response.code() + "").equals("200")) {
            //sucess
            messageRs.setStatus("OK");
            messageRs.setData(billerRsList);

            logger.info(responseJson);

        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return messageRs;
    }


    @RequestMapping(value = "/schoolBillInquiry", method = RequestMethod.GET)
    public InquiryRs schoolBillInquiry(HttpServletRequest httpServletRequest,
                                       @RequestParam(value = "subscriptionNumber", required = false) String billNumber) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        String code = httpServletRequest.getHeader("code");
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, "");
        Request request = new Request.Builder()
                .url(environment.getProperty("ebill_url") + "/ac/bill?billNumber=" + billNumber)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        InquiryRs inquiryRs = new InquiryRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            inquiryRs.setStatus("OK");
            inquiryRs.setBillerId(responseJson.getJSONObject("body").getString("billerCode"));
            inquiryRs.setBillerName(responseJson.getJSONObject("body").getString("billerName"));
            inquiryRs.setBillNumber(responseJson.getJSONObject("body").getString("sadadNo"));
            inquiryRs.setAmountDue(String.valueOf(responseJson.getJSONObject("body").getDouble("amountDue")));
            inquiryRs.setServiceName(responseJson.getJSONObject("body").isNull("serviceName") ? "" : responseJson.getJSONObject("body").getString("serviceName"));
            inquiryRs.setBillStatus(responseJson.getJSONObject("body").getString("invoiceStatusCaption"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return inquiryRs;
    }


    @RequestMapping(value = "/schoolBillPayment", method = RequestMethod.POST)
    public PaymentRs schoolBillPayment(HttpServletRequest httpServletRequest,
                                       @RequestBody PaymentRq paymentRq) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("statusCode", paymentRq.getStatus());
        jsonObject.put("bankTransactionRef", paymentRq.getReference());
        jsonObject.put("destinationReference", paymentRq.getBillNumber());
        jsonObject.put("description", paymentRq.getDescription());
//        jsonObject.put("source", paymentRq.getSource());
//        jsonObject.put("sourceReference", paymentRq.getSourceReference());

        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/billPayment")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        PaymentRs paymentRs = new PaymentRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            paymentRs.setStatus("OK");
            paymentRs.setT24Reference(paymentRq.getReference());
            paymentRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return paymentRs;
    }


    @RequestMapping(value = "/sendOtpEcommerce", method = RequestMethod.POST)
    public OTPRs sendOtpEcommerce(HttpServletRequest httpServletRequest,
                                  @RequestBody OTPRq otpRq) {

        String username = httpServletRequest.getHeader("username");
        String password = httpServletRequest.getHeader("password");

        logger.info("username: " + username);
        logger.info("password: " + password);

        if (username == null
                || password == null
                || otpRq.getAmount() == null
                || otpRq.getMobileNumber() == null)
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Bad Request");

        OkHttpClient client = new OkHttpClient();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("mobileNo", otpRq.getMobileNumber());
        jsonObject.put("transactionAmount", otpRq.getAmount() + "");

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/sendOtpEcommerce")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("password", password)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        OTPRs otpRs = new OTPRs();
        if ((response.code() + "").equals("201") || (response.code() + "").equals("200")) {
            //sucess
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            otpRs.setStatus("OK");
            otpRs.setMobileNumber(responseJson.getJSONObject("body").getString("mobileNo"));
            otpRs.setExpDate(LocalDateTime.of(
                    responseJson.getJSONObject("body").getJSONObject("expDate").getInt("year"),
                    responseJson.getJSONObject("body").getJSONObject("expDate").getInt("monthValue"),
                    responseJson.getJSONObject("body").getJSONObject("expDate").getInt("dayOfMonth"),
                    responseJson.getJSONObject("body").getJSONObject("expDate").getInt("hour"),
                    responseJson.getJSONObject("body").getJSONObject("expDate").getInt("second")
            ).toString());

        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return otpRs;
    }

    @RequestMapping(value = "/paymentEcommerce", method = RequestMethod.POST, consumes = {"text/plain", "application/json"})
    public EcommerceRs paymentEcommerce(HttpServletRequest httpServletRequest,
                                        @RequestBody String stringRequest) {

        String username = httpServletRequest.getHeader("username");
        String password = httpServletRequest.getHeader("password");

        logger.info("username: " + username);
        logger.info("password: " + password);

        System.out.println("request: " + stringRequest);

        logger.info("XXX: " + stringRequest);

        JSONObject jsonObjectRequest = null;
        try {
            jsonObjectRequest = new JSONObject(stringRequest);
        } catch (Exception e) {
            logger.error("EX: " + e.getMessage());
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Error Parse the JSON: " + stringRequest);
        }

        EcommerceRq ecommerceRq = new EcommerceRq();
        ecommerceRq.setReference(jsonObjectRequest.getString("reference"));
        ecommerceRq.setMobileNumber(jsonObjectRequest.getString("mobileNumber"));
        ecommerceRq.setAmount(Double.parseDouble(jsonObjectRequest.getString("amount")));
        ecommerceRq.setOtpCode(jsonObjectRequest.getString("otpCode"));


        if (username == null
                || password == null
                || ecommerceRq.getAmount() == null
                || ecommerceRq.getReference() == null
                || ecommerceRq.getMobileNumber() == null
                || ecommerceRq.getOtpCode() == null)
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Bad Request");

        OkHttpClient client = new OkHttpClient();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("destinationAmount", ecommerceRq.getAmount());
        jsonObject.put("destinationWalletCode", ecommerceRq.getMobileNumber());
        jsonObject.put("bankTransactionRef", ecommerceRq.getReference());
        jsonObject.put("otpCode", ecommerceRq.getOtpCode());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/paymentEcommerce")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("password", password)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        EcommerceRs ecommerceRs = new EcommerceRs();
        if ((response.code() + "").equals("201") || (response.code() + "").equals("200")) {
            //sucess
            ecommerceRs.setStatus("OK");
            ecommerceRs.setTransactionReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return ecommerceRs;
    }


    @RequestMapping(value = "/generate/excel", method = RequestMethod.GET)
    public FileRs generateExcel(HttpServletRequest httpServletRequest) {
//
//        String username = httpServletRequest.getHeader("username");
//        String password = httpServletRequest.getHeader("password");
        String code = httpServletRequest.getHeader("code");

        logger.info("code: " + code);

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/generate/excel")
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
//                .addHeader("username", username.toLowerCase())
//                .addHeader("password", password)
                .addHeader("code", code)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        FileRs fileRs = new FileRs();
        if ((response.code() + "").equals("201") || (response.code() + "").equals("200")) {
            //sucess
            fileRs.setStatus("OK");
            fileRs.setName(responseJson.getJSONObject("body").getString("name"));
            fileRs.setBase64(responseJson.getJSONObject("body").getString("base64"));
//            fileRs.setType(responseJson.getJSONObject("body").getString("type"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return fileRs;

    }


    @RequestMapping(value = "/upload/salary/excel", method = RequestMethod.POST)
    public FileRs uploadSalaryExcel(HttpServletRequest httpServletRequest,
                                    @RequestBody FileRq fileRq) {
//        String username = httpServletRequest.getHeader("username");
//        String password = httpServletRequest.getHeader("password");
        String code = httpServletRequest.getHeader("code");

        logger.info("code: " + code);

        if (code == null
                || fileRq.getBase64() == null)
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Bad Request");

        OkHttpClient client = new OkHttpClient();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("base64", fileRq.getBase64());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/upload/salary/excel")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", code)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        FileRs fileRs = new FileRs();
        if ((response.code() + "").equals("201") || (response.code() + "").equals("200")) {
            //sucess
            fileRs.setStatus("OK");
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return fileRs;
    }

    @RequestMapping(value = "/upload/excel/airTime", method = RequestMethod.POST)
    public FileRs uploadExcelAirTime(HttpServletRequest httpServletRequest,
                                     @RequestBody FileRq fileRq) {
        String code = httpServletRequest.getHeader("code");

        if (code == null
                || fileRq.getBase64() == null)
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "Bad Request");

        OkHttpClient client = new OkHttpClient();

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("base64", fileRq.getBase64());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/upload/excel/airTime")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", code)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        FileRs fileRs = new FileRs();
        if ((response.code() + "").equals("201") || (response.code() + "").equals("200")) {
            //sucess
            fileRs.setStatus("OK");
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return fileRs;
    }


    @RequestMapping(value = "/otcSend", method = RequestMethod.POST)
    public OTCSendRs otcSend(HttpServletRequest httpServletRequest,
                             @RequestBody OTCSendRq otcSendRq) {
        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);


        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }


        String code = httpServletRequest.getHeader("branchCode");
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", otcSendRq.getStatus());
        jsonObject.put("reference", otcSendRq.getReference());
        jsonObject.put("amount", otcSendRq.getAmount());
        jsonObject.put("senderFullName", otcSendRq.getSenderFullName());
        jsonObject.put("senderIdentityNo", otcSendRq.getSenderIdentityNo());
        jsonObject.put("senderMobileNo", otcSendRq.getSenderMobileNo());
        jsonObject.put("receiverFullName", otcSendRq.getReceiverFullName());
        jsonObject.put("receiverIdentityNo", otcSendRq.getReceiverIdentityNo());
        jsonObject.put("receiverMobileNo", otcSendRq.getReceiverMobileNo());
        jsonObject.put("description", otcSendRq.getDescription());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/otc/send")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", code)
                .addHeader("username", username.toLowerCase())
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        OTCSendRs otcSendRs = new OTCSendRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            otcSendRs.setStatus("OK");
            otcSendRs.setT24Reference(otcSendRq.getReference());
            otcSendRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return otcSendRs;
    }


    @RequestMapping(value = "/otc/inquire", method = RequestMethod.GET)
    public OTCInquireRs otcInquireRs(HttpServletRequest httpServletRequest,
                                     @RequestParam(value = "txnReference", required = false) String txnReference) {

        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        logger.info("XXX: " + txnReference);

        OkHttpClient client = new OkHttpClient();


        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/otc/find?reference=" + txnReference)
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", branchCode)
                .addHeader("username", username.toLowerCase())
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        OTCInquireRs otcInquireRs = new OTCInquireRs();

        if ((response.code() + "").equals("200") || (response.code() + "").equals("200")) {
            //sucess
            otcInquireRs.setSenderFullName(responseJson.getJSONObject("body").getString("senderFullName"));
            otcInquireRs.setSenderMobileNo(!responseJson.getJSONObject("body").isNull("senderMobileNo") ? responseJson.getJSONObject("body").getString("senderMobileNo") : null);
            otcInquireRs.setSenderIdentityNo(!responseJson.getJSONObject("body").isNull("senderIdentityNo") ? responseJson.getJSONObject("body").getString("senderIdentityNo") : null);
            otcInquireRs.setReceiverFullName(responseJson.getJSONObject("body").getString("receiverFullName"));
            otcInquireRs.setReceiverMobileNo(!responseJson.getJSONObject("body").isNull("receiverMobileNo") ? responseJson.getJSONObject("body").getString("receiverMobileNo") : null);
            otcInquireRs.setReceiverIdentityNo(!responseJson.getJSONObject("body").isNull("receiverIdentityNo") ? responseJson.getJSONObject("body").getString("receiverIdentityNo") : null);
            if (!responseJson.getJSONObject("body").isNull("senderBankBranchCode")) {
                otcInquireRs.setSenderBranchCode(responseJson.getJSONObject("body").getString("senderBankBranchCode"));
            }
            if (!responseJson.getJSONObject("body").isNull("receiverBankBranchCode")) {
                otcInquireRs.setReceiverBranchCode(responseJson.getJSONObject("body").getString("receiverBankBranchCode"));
            }

            if (!responseJson.getJSONObject("body").isNull("statusCode")) {
                otcInquireRs.setStatus(responseJson.getJSONObject("body").getString("statusCode"));
            }

            if (!responseJson.getJSONObject("body").isNull("amount")) {
                otcInquireRs.setAmount(responseJson.getJSONObject("body").get("amount").toString());
            }

//            otcInquireRs.setAmount(responseJson.getJSONObject("body").getString("destinationAmountRnd"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return otcInquireRs;
    }


    @RequestMapping(value = "/otcReceive", method = RequestMethod.POST)
    public OTCSendRs otcReceive(HttpServletRequest httpServletRequest,
                                @RequestBody OTCSendRq otcSendRq) {

        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }


        String code = httpServletRequest.getHeader("branchCode");
        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("statusCode", "NEW");
        jsonObject.put("internalReference", otcSendRq.getTransferNumber());
        jsonObject.put("bankTransactionRef", otcSendRq.getReference());//Description
        jsonObject.put("description", otcSendRq.getDescription());//

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/otc/receive")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("code", code)
                .addHeader("username", username.toLowerCase())
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        OTCSendRs otcSendRs = new OTCSendRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            otcSendRs.setStatus("OK");
            otcSendRs.setT24Reference(otcSendRq.getReference());
            otcSendRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return otcSendRs;
    }


    @RequestMapping(value = "/airTime/single", method = RequestMethod.POST)
    public AirTimeRs airTimeSingle(HttpServletRequest httpServletRequest,
                                   @RequestBody AirTimeRq airTimeRq) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("statusCode", airTimeRq.getStatus());
        jsonObject.put("bankTransactionRef", airTimeRq.getReference());
        jsonObject.put("destinationReference", airTimeRq.getMobileNo());
        jsonObject.put("description", airTimeRq.getDescription());
        jsonObject.put("destinationAmount", airTimeRq.getAmount());
//        jsonObject.put("source", paymentRq.getSource());
//        jsonObject.put("sourceReference", paymentRq.getSourceReference());

        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonObject.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/airTime/single")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        AirTimeRs airTimeRs = new AirTimeRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            airTimeRs.setStatus("OK");
            airTimeRs.setT24Reference(airTimeRq.getReference());
            airTimeRs.setLiyuPayReference(responseJson.getJSONObject("body").getString("internalReference"));
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return airTimeRs;
    }


    @RequestMapping(value = "/airTime/bulk", method = RequestMethod.POST)
    public List<AirTimeRs> airTimeBulk(HttpServletRequest httpServletRequest,
                                       @RequestBody List<AirTimeRq> airTimeRqList) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        MediaType mediaType = MediaType.parse("application/json");

        JSONArray jsonArray = new JSONArray();

        for (AirTimeRq airTimeRq : airTimeRqList) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("statusCode", airTimeRq.getStatus());
            jsonObject.put("bankTransactionRef", airTimeRq.getReference());
            jsonObject.put("destinationReference", airTimeRq.getMobileNo());
            jsonObject.put("description", airTimeRq.getDescription());
            jsonObject.put("destinationAmount", airTimeRq.getAmount());

            jsonArray.put(jsonObject);
        }


        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonArray.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/airTime/bulk")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        List<AirTimeRs> airTimeRsList = new ArrayList<>();

        System.out.println("responseJson: " + responseJson.toString());

        if ((response.code() + "").equals("200")) {
            //sucess
            JSONArray responseArray = (responseJson.getJSONArray("body"));

            for (int i = 0; i < responseArray.length(); i++) {

                JSONObject result = responseArray.getJSONObject(i);

                AirTimeRs airTimeRs = new AirTimeRs();
                if (!result.isNull("internalReference")) {
                    //success
                    airTimeRs.setStatus("OK");
                    airTimeRs.setT24Reference(result.getString("bankTransactionRef"));
                    airTimeRs.setLiyuPayReference(result.getString("internalReference"));
                } else {
                    //error
                    airTimeRs.setStatus("ERROR");
                    airTimeRs.setT24Reference(result.getString("bankTransactionRef"));
                    airTimeRs.setLiyuPayReference("");
                }

                airTimeRsList.add(airTimeRs);
            }

        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return airTimeRsList;
    }


    @RequestMapping(value = "/salary/bulk", method = RequestMethod.POST)
    public List<CashinRs> salaryBulk(HttpServletRequest httpServletRequest, @RequestBody List<CashinRq> cashinRqList) throws ServiceFaultException {


        String username = httpServletRequest.getHeader("username");
        String branchCode = httpServletRequest.getHeader("branchCode");

        logger.info("username: " + username);
        logger.info("branchCode: " + branchCode);

        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (branchCode == null || branchCode.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.UNAUTHORIZED, "missing branch code");
        }

        OkHttpClient client = new OkHttpClient();

        client.setConnectTimeout(15, TimeUnit.SECONDS);
        client.setReadTimeout(15, TimeUnit.SECONDS);

        JSONArray jsonArray = new JSONArray();

        for (CashinRq cashinRq : cashinRqList) {

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("destinationAmount", cashinRq.getAmount());
            jsonObject.put("bankTransactionRef", cashinRq.getReference());
            jsonObject.put("statusCode", cashinRq.getStatus());
            jsonObject.put("destinationReference", cashinRq.getDestinationReference());
            jsonObject.put("description", cashinRq.getDescription());

            jsonArray.put(jsonObject);

        }

        logger.info("jsonObject: " + jsonArray.toString());

        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody reqBody = com.squareup.okhttp.RequestBody.create(mediaType, jsonArray.toString());
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/salary/bulk")
                .post(reqBody)
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("username", username.toLowerCase())
                .addHeader("code", branchCode)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        List<CashinRs> cashinRsList = new ArrayList<>();

        if ((response.code() + "").equals("200")) {

            JSONArray responseArray = (responseJson.getJSONArray("body"));

            for (int i = 0; i < responseArray.length(); i++) {

                JSONObject result = responseArray.getJSONObject(i);

                CashinRs cashinRs = new CashinRs();
                if (!result.isNull("internalReference")) {
                    //success
                    cashinRs.setStatus("OK");
                    cashinRs.setT24Reference(result.getString("bankTransactionRef"));
                    cashinRs.setLiyuPayReference(result.getString("internalReference"));
                } else {
                    //error
                    cashinRs.setStatus("ERROR");
                    cashinRs.setT24Reference(result.getString("bankTransactionRef"));
                    cashinRs.setLiyuPayReference("");
                }

                cashinRsList.add(cashinRs);
            }
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }


        return cashinRsList;
    }

    @RequestMapping(value = "/merchant/qr", method = RequestMethod.GET)
    public MerchantQRRs getMerchantQR(HttpServletRequest httpServletRequest) {
        String username = httpServletRequest.getHeader("username");
        String password = httpServletRequest.getHeader("password");


        if (username == null || username.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing username");
        }

        if (password == null || password.isEmpty()) {
            throw new ServiceFaultException(HttpStatus.BAD_REQUEST, "missing password");
        }

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(environment.getProperty("dw_url") + "/inetgration/merchant/QR")
                .get()
                .addHeader("Content-Type", "application/json")
                .addHeader("cache-control", "no-cache")
                .addHeader("password", password)
                .addHeader("username", username)
                .build();

        Response response = null;
        JSONObject responseJson = null;
        try {
            response = client.newCall(request).execute();
            responseJson = new JSONObject(response.body().string());
            logger.info("responseJson: " + responseJson.toString());
        } catch (Exception e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
        }

        MerchantQRRs merchantQRRs = new MerchantQRRs();

        if ((response.code() + "").equals("200")) {
            //sucess
            merchantQRRs.setStatus("OK");
            merchantQRRs.setCode(responseJson.getJSONObject("body").getString("code"));
            merchantQRRs.setDescription(responseJson.getJSONObject("body").getString("description"));
            merchantQRRs.setSerial(responseJson.getJSONObject("body").get("serial").toString());
        } else {
            throw new ServiceFaultException(HttpStatus.valueOf(response.code()), responseJson.getString("text"));
        }

        return merchantQRRs;

    }


}
