package com.dc.Integration.boa.controllers;

import com.dc.Integration.boa.pojo.*;


import com.dc.Integration.boa.exception.ServiceFaultException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;

@RestController
@RequestMapping("/boa")
public class BoaController {

    private static final Logger logger = LogManager.getLogger("APP_INTG");


    @Value("${service_timeout}")
    private Integer timeOut;

    @Autowired
    private Environment environment;

    @RequestMapping(value = "/addBenfi", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String addBenfi(@RequestBody BeneficiaryRq beneficiaryRq) throws ServiceFaultException {

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgc=\"http://temenos.com/DGCASH\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgc:BENFICAIRYACCOUNTNAME>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <BENACCOUNTType>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <enquiryInputCollection>\n" +
                    "               <!--Optional:-->\n" +
                    "               <columnName>ACCOUNT.NUMBER</columnName>\n" +
                    "               <!--Optional:-->\n" +
                    "               <criteriaValue>" + beneficiaryRq.getCreditAccNo() + "</criteriaValue>\n" +
                    "               <!--Optional:-->\n" +
                    "               <operand>EQ</operand>\n" +
                    "            </enquiryInputCollection>\n" +
                    "         </BENACCOUNTType>\n" +
                    "      </dgc:BENFICAIRYACCOUNTNAME>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("add Benfi Response: " + outputString);

            return outputString;

//            BeneficiaryRs beneficiaryRs = new BeneficiaryRs();
//
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder;
//            InputSource is;
//            NodeList tra = null;
//
//            builder = factory.newDocumentBuilder();
//            is = new InputSource(new StringReader(outputString));
//            Document doc = builder.parse(is);
//
//            tra = doc.getElementsByTagName("benCustomer");
//
//            logger.info(tra.item(0).getTextContent());
//
//            beneficiaryRs.setBenCustomer(tra.item(0).getTextContent());
//            return beneficiaryRs;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.NOT_FOUND, "error");
        }


    }

    @RequestMapping(value = "/linkWithT24", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String linkWithT24(@RequestBody WalletRegestreationRq walletRegestreationRq) throws ServiceFaultException {


        logger.info("dataLinkWithT24Rq: " + walletRegestreationRq.toString());

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgc=\"http://temenos.com/DGCASH\" xmlns:boaw=\"http://temenos.com/BOAWALLETISSUE1REG\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgc:WALLETREGISTRATION>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <OfsFunction>\n" +
                    "         </OfsFunction>\n" +
                    "         <BOAWALLETISSUE1REGType id=\"" + walletRegestreationRq.getWalletNumber() + "\">\n" +
                    "            <!--Optional:-->\n" +
                    "            <boaw:ACCOUNTNO>" + walletRegestreationRq.getBoaAccountNo() + "</boaw:ACCOUNTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <boaw:MOBILENO>" + walletRegestreationRq.getWalletNumber() + "</boaw:MOBILENO>\n" +
                    "         </BOAWALLETISSUE1REGType>\n" +
                    "      </dgc:WALLETREGISTRATION>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("linkWithT24 Response: " + outputString);

            return outputString;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "error while calling service");
        }
    }


    @RequestMapping(value = "/transfer", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String consumeTransfer(@RequestBody BankDataTransferRq bankDataTransferRq) throws
            ServiceFaultException {


        logger.info("dataTransferRq: " + bankDataTransferRq.toString());

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgc=\"http://temenos.com/DGCASH\" xmlns:fun=\"http://temenos.com/FUNDSTRANSFERDGWAL2AC\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgc:WALLETTOACC>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <OfsFunction>\n" +
                    "         </OfsFunction>\n" +
                    "         <FUNDSTRANSFERDGWAL2ACType id=\"\">\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:TRANSACTIONTYPE></fun:TRANSACTIONTYPE>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITACCTNO>" + bankDataTransferRq.getDebitAccNo() + "</fun:DEBITACCTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITCURRENCY>" + bankDataTransferRq.getDebitCurrency() + "</fun:DEBITCURRENCY>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITAMOUNT>" + roundDecimal(bankDataTransferRq.getDebitAmount()) + "</fun:DEBITAMOUNT>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITTHEIRREF>" + bankDataTransferRq.getDebitTheIRREF() + "</fun:DEBITTHEIRREF>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:CREDITACCTNO>" + bankDataTransferRq.getCreditAccNo() + "</fun:CREDITACCTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:gORDERINGBANK g=\"1\">\n" +
                    "               <!--Zero or more repetitions:-->\n" +
                    "               <fun:ORDERINGBANK></fun:ORDERINGBANK>\n" +
                    "            </fun:gORDERINGBANK>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DGTRANSACTIONID>" + bankDataTransferRq.getTransactionId() + "</fun:DGTRANSACTIONID>\n" +
                    "         </FUNDSTRANSFERDGWAL2ACType>\n" +
                    "      </dgc:WALLETTOACC>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("transfer response: " + outputString);

            //
            return outputString;

//            BankDataTransferRs bankDataTransferRs = new BankDataTransferRs();
//
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder;
//            InputSource is;
//            NodeList status = null;
//            NodeList tra = null;
//
//            builder = factory.newDocumentBuilder();
//            is = new InputSource(new StringReader(outputString));
//            Document doc = builder.parse(is);
//            status = doc.getElementsByTagName("successIndicator");
//            tra = doc.getElementsByTagName("transactionId");
//            logger.info(status.item(0).getTextContent());
//            logger.info(tra.item(0).getTextContent());
//            bankDataTransferRs.setSuccessIndicator(status.item(0).getTextContent());
//            bankDataTransferRs.setTransactionId(tra.item(0).getTextContent());
//            return bankDataTransferRs;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.NOT_FOUND, "errror");
        }

    }


    @RequestMapping(value = "/loadMoney", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String loadMoney(@RequestBody MoneyLoadRq moneyLoadRq) throws ServiceFaultException {
        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgc=\"http://temenos.com/DGCASH\" xmlns:fun=\"http://temenos.com/FUNDSTRANSFERDGWALMNYLOAD\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgc:DGMONEYLOAD>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <OfsFunction>\n" +
                    "         </OfsFunction>\n" +
                    "         <FUNDSTRANSFERDGWALMNYLOADType id=\"\">\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:TRANSACTIONTYPE></fun:TRANSACTIONTYPE>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITACCTNO>" + moneyLoadRq.getDebitAccNo() + "</fun:DEBITACCTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITCURRENCY>ETB</fun:DEBITCURRENCY>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITAMOUNT>" + moneyLoadRq.getDebitAmount() + "</fun:DEBITAMOUNT>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DEBITTHEIRREF></fun:DEBITTHEIRREF>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:CREDITTHEIRREF>" + moneyLoadRq.getCreditTheIRREF() + "</fun:CREDITTHEIRREF>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:CREDITACCTNO></fun:CREDITACCTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <fun:DGTRANSACTIONID>" + moneyLoadRq.getTransactionId() + "</fun:DGTRANSACTIONID>\n" +
                    "         </FUNDSTRANSFERDGWALMNYLOADType>\n" +
                    "      </dgc:DGMONEYLOAD>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("money load: " + outputString);

            return outputString;

//            MoneyLoadRs bankDataTransferRs = new MoneyLoadRs();
//
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder;
//            InputSource is;
//
//
//            builder = factory.newDocumentBuilder();
//            is = new InputSource(new StringReader(outputString));
//
//            return bankDataTransferRs;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.NOT_FOUND, "error");
        }

    }


    @RequestMapping(value = "/airTime", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String airTimeTopup(@RequestBody AirTimeRq airTimeRq) throws ServiceFaultException {
        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("air_time_top_utl");
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();
            String jsonInput = "{\n" +
                    "\t\"Username\": \"" + this.environment.getProperty("airtime_username") + "\",\n" +
                    "\t\"Password\": \"" + this.environment.getProperty("airtime_password") + "\",\n" +
                    "\t\"MSISDN2\": \"" + airTimeRq.getMobileNo() + "\",\n" +
                    "\t\"AMOUNT\": \"" + airTimeRq.getAmount() + "\",\n" +
                    "\t\"FT\": \"" + airTimeRq.getTransactionId() + "\"\n" +
                    "}\n";


            logger.info("jsonInput: " + jsonInput);

            byte[] buffer = new byte[jsonInput.length()];
            buffer = jsonInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "application/json; charset=utf-8");
//            httpConn.setRequestProperty("SOAPAction", SOAPAction);
//            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("air time response: " + outputString);

            return outputString;

//            MoneyLoadRs bankDataTransferRs = new MoneyLoadRs();
//
//            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
//            DocumentBuilder builder;
//            InputSource is;
//
//
//            builder = factory.newDocumentBuilder();
//            is = new InputSource(new StringReader(outputString));
//
//            return bankDataTransferRs;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.NOT_FOUND, "error");
        }

    }


    @RequestMapping(value = "/BusinesslinkWithT24", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String BusinesslinkWithT24(@RequestBody WalletRegestreationRq walletRegestreationRq) throws ServiceFaultException {


        logger.info("BuisnessLinkWithT24Rq: " + walletRegestreationRq.toString());

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgc=\"http://temenos.com/DGCASH\" xmlns:boam=\"http://temenos.com/BOAMERCHANTREGISTRATIONREG\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgc:MERCHANTREGISTRTION>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <OfsFunction/>\n" +
                    "         <BOAMERCHANTREGISTRATIONREGType id=\"" + walletRegestreationRq.getWalletNumber() + "\">\n" +
                    "            <!--Optional:-->\n" +
                    "            <boam:gACCOUNTNO g=\"1\">\n" +
                    "               <!--Zero or more repetitions:-->\n" +
                    "               <boam:ACCOUNTNO>" + walletRegestreationRq.getBoaAccountNo() + "</boam:ACCOUNTNO>\n" +
                    "            </boam:gACCOUNTNO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <boam:MOBILENO>" + walletRegestreationRq.getMobileNumber() + "</boam:MOBILENO>\n" +
                    "            <!--Optional:-->\n" +
                    "            <boam:MERTYPE>" + walletRegestreationRq.getAccountType() + "</boam:MERTYPE>\n" +
                    "         </BOAMERCHANTREGISTRATIONREGType>\n" +
                    "      </dgc:MERCHANTREGISTRTION>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("linkWithT24 Response: " + outputString);

            return outputString;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "error while calling service");
        }
    }


    @RequestMapping(value = "/CheckTransaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String CheckTransactionStatusWithT24(@RequestBody TransactionStatusRq transactionStatusRq) throws ServiceFaultException {


        logger.info("CheckTransactionRq: " + transactionStatusRq.toString());

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgw=\"http://temenos.com/DGCASH\">\n" +
                    "   <soapenv:Header/>\n" +
                    "   <soapenv:Body>\n" +
                    "      <dgw:TRANASCTIONSTATUSCHECK>\n" +
                    "         <WebRequestCommon>\n" +
                    "            <!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "         </WebRequestCommon>\n" +
                    "         <BOAINTSTATUSCHECKType>\n" +
                    "            <!--Zero or more repetitions:-->\n" +
                    "            <enquiryInputCollection>\n" +
                    "               <!--Optional:-->\n" +
                    "               <columnName>@ID</columnName>\n" +
                    "               <!--Optional:-->\n" +
                    "               <criteriaValue>" + transactionStatusRq.getCriteriaValue() + "</criteriaValue>\n" +
                    "               <!--Optional:-->\n" +
                    "               <operand>EQ</operand>\n" +
                    "            </enquiryInputCollection>\n" +
                    "         </BOAINTSTATUSCHECKType>\n" +
                    "      </dgw:TRANASCTIONSTATUSCHECK>\n" +
                    "   </soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                logger.info("httpConn code: "+httpConn.getResponseCode());
                logger.info("httpConn message: "+httpConn.getResponseMessage());
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("linkWithT24 Response: " + outputString);

            return outputString;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "error while calling service");
        }
    }

    @RequestMapping(value = "/ReverseTransaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_XML_VALUE)
    public String ReverseTransaction(@RequestBody TransactionStatusRq transactionStatusRq) {

        logger.info("CheckTransactionRq: " + transactionStatusRq.toString());

        //call soap service
        try {

            String responseString = "";
            String outputString = "";
            String wsURL = environment.getProperty("boa_url") + "/DGWALLET/services";
            logger.info("wsURL: " + wsURL);
            URL url = new URL(wsURL);
            URLConnection connection = url.openConnection();
            HttpURLConnection httpConn = (HttpURLConnection) connection;
            httpConn.setConnectTimeout(timeOut);
            ByteArrayOutputStream bout = new ByteArrayOutputStream();

            String xmlInput = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:dgw=\"http://temenos.com/DGWALLET\">\n" +
                    "<soapenv:Header/>\n" +
                    "<soapenv:Body>\n" +
                    "<dgw:TRANSACTIONREVERSAL>\n" +
                    "<WebRequestCommon>\n" +
                    "<!--Optional:-->\n" +
                    "            <company>" + this.environment.getProperty("boa_company") + "</company>\n" +
                    "            <password>" + this.environment.getProperty("boa_password") + "</password>\n" +
                    "            <userName>" + this.environment.getProperty("boa_username") + "</userName>\n" +
                    "</WebRequestCommon>\n" +
                    "<OfsFunction/>\n" +
                    "<!--Optional:-->\n" +
                    "<FUNDSTRANSFERDGWAL2ACType>\n" +
                    "<!--Optional:-->\n" +
                    "<transactionId>" + transactionStatusRq.getTransactionId() + "</transactionId>\n" +
                    "</FUNDSTRANSFERDGWAL2ACType>\n" +
                    "</dgw:TRANSACTIONREVERSAL>\n" +
                    "</soapenv:Body>\n" +
                    "</soapenv:Envelope>";

            logger.info("xmlInput: " + xmlInput);

            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes("UTF-8");
            bout.write(buffer);
            byte[] b = bout.toByteArray();
            String SOAPAction = "";

            httpConn.setRequestProperty("Content-Length",
                    String.valueOf(b.length));
            httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
            httpConn.setRequestProperty("SOAPAction", SOAPAction);
            httpConn.setRequestProperty("MIME-Version", " 1.0");
            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            logger.info("httpConn start connection ...............");
            InputStreamReader isr = null;
            if (httpConn.getResponseCode() == 200) {
                logger.info("httpConn true");
                isr = new InputStreamReader(httpConn.getInputStream());
            } else {
                logger.info("httpConn false");
                isr = new InputStreamReader(httpConn.getErrorStream());
            }

            BufferedReader in = new BufferedReader(isr);
            logger.info("BufferedReader response ....");
            while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }

            logger.info("linkWithT24 Response: " + outputString);

            return outputString;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e), e);
            throw new ServiceFaultException(HttpStatus.INTERNAL_SERVER_ERROR, "error while calling service");
        }
    }

    public String roundDecimal(Double value) {
        if (value == null) {
            return "00.00";
        }
        DecimalFormat df = new DecimalFormat("0.00");
        return df.format(value);
    }

}
