package com.dc.Integration.boa.service;

import org.zalando.logbook.Correlation;

import java.io.IOException;

public interface LogService {

    public void logHttp(Correlation<String, String> correlation) throws IOException;

}
