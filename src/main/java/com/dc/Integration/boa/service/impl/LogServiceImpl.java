package com.dc.Integration.boa.service.impl;

import com.dc.Integration.boa.service.LogService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.zalando.logbook.Correlation;

import java.io.IOException;

@Service
public class LogServiceImpl implements LogService {

    private static final Logger logger = LogManager.getLogger("HTTP_CORE");

    @Override
    public void logHttp(Correlation<String, String> correlation) throws IOException {

        if(correlation.getOriginalRequest().getBodyAsString() != null && correlation.getOriginalRequest().getBodyAsString().equals("<skipped>")){
            return;
        }
        JSONObject httpRequestAndResponse = new JSONObject();
        httpRequestAndResponse.put("method", correlation.getOriginalRequest().getMethod());
        httpRequestAndResponse.put("url", correlation.getOriginalRequest().getPath());
        httpRequestAndResponse.put("address", correlation.getOriginalRequest().getHost());
        httpRequestAndResponse.put("request", correlation.getOriginalRequest().getBodyAsString());
        httpRequestAndResponse.put("response", correlation.getOriginalResponse().getBodyAsString());
        httpRequestAndResponse.put("status", correlation.getOriginalResponse().getStatus());
        httpRequestAndResponse.put("duration",correlation.getDuration().getNano());

        logger.info(httpRequestAndResponse.toString());
    }
}