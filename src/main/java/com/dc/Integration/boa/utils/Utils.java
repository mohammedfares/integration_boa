package com.dc.Integration.boa.utils;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

public class Utils {

    public static OkHttpClient setTimeOut(Integer time, OkHttpClient client,TimeUnit timeUnit) {
        client.setConnectTimeout(time,timeUnit );
        client.setReadTimeout(time, timeUnit);
        client.setWriteTimeout(time, timeUnit);
        return client;
    }
}
