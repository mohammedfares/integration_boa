package com.dc.Integration.boa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.swing.text.Element;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.IOException;
import java.io.StringReader;

@SpringBootApplication
public class IntegrationBoaApplication {

	public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
		SpringApplication.run(IntegrationBoaApplication.class, args);

		String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
				"<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
				"   <S:Body>\n" +
				"      <ns2:WALLETACCTOACCResponse xmlns:ns2=\"T24WebServicesImpl\">\n" +
				"         <Status>\n" +
				"            <transactionId>FT172333Z1RX</transactionId>\n" +
				"            <messageId/>\n" +
				"            <successIndicator>Success</successIndicator>\n" +
				"            <application>FUNDS.TRANSFER</application>\n" +
				"         </Status>\n" +
				"         <FUNDSTRANSFERType id=\"FT172333Z1RX\">\n" +
				"            <TRANSACTIONTYPE>AC</TRANSACTIONTYPE>\n" +
				"            <DEBITACCTNO>1114174</DEBITACCTNO>\n" +
				"            <CURRENCYMKTDR>1</CURRENCYMKTDR>\n" +
				"            <DEBITCURRENCY>ETB</DEBITCURRENCY>\n" +
				"            <DEBITAMOUNT>100.00</DEBITAMOUNT>\n" +
				"            <DEBITVALUEDATE>20170821</DEBITVALUEDATE>\n" +
				"            <DEBITTHEIRREF>TEST AC 2 AC</DEBITTHEIRREF>\n" +
				"            <CREDITTHEIRREF>987654</CREDITTHEIRREF>\n" +
				"            <CREDITACCTNO>132</CREDITACCTNO>\n" +
				"            <CURRENCYMKTCR>1</CURRENCYMKTCR>\n" +
				"            <CREDITCURRENCY>ETB</CREDITCURRENCY>\n" +
				"            <CREDITVALUEDATE>20170821</CREDITVALUEDATE>\n" +
				"            <PROCESSINGDATE>20170821</PROCESSINGDATE>\n" +
				"            <CHARGECOMDISPLAY>NO</CHARGECOMDISPLAY>\n" +
				"            <COMMISSIONCODE>DEBIT PLUS CHARGES</COMMISSIONCODE>\n" +
				"            <CHARGECODE>DEBIT PLUS CHARGES</CHARGECODE>\n" +
				"            <PROFITCENTRECUST>121270</PROFITCENTRECUST>\n" +
				"            <RETURNTODEPT>NO</RETURNTODEPT>\n" +
				"            <FEDFUNDS>NO</FEDFUNDS>\n" +
				"            <POSITIONTYPE>TR</POSITIONTYPE>\n" +
				"            <AMOUNTDEBITED>ETB100.00</AMOUNTDEBITED>\n" +
				"            <AMOUNTCREDITED>ETB100.00</AMOUNTCREDITED>\n" +
				"            <gDELIVERYOUTREF>\n" +
				"               <DELIVERYOUTREF>D20180614036936462701-900.1.1       DEBIT ADVICE</DELIVERYOUTREF>\n" +
				"               <DELIVERYOUTREF>D20180614036936462702-910.2.1       CREDIT ADVICE</DELIVERYOUTREF>\n" +
				"            </gDELIVERYOUTREF>\n" +
				"            <CREDITCOMPCODE>ET0010009</CREDITCOMPCODE>\n" +
				"            <DEBITCOMPCODE>ET0010051</DEBITCOMPCODE>\n" +
				"            <LOCAMTDEBITED>100.00</LOCAMTDEBITED>\n" +
				"            <LOCAMTCREDITED>100.00</LOCAMTCREDITED>\n" +
				"            <CUSTGROUPLEVEL>99</CUSTGROUPLEVEL>\n" +
				"            <DEBITCUSTOMER>121270</DEBITCUSTOMER>\n" +
				"            <CREDITCUSTOMER>100001</CREDITCUSTOMER>\n" +
				"            <DRADVICEREQDYN>Y</DRADVICEREQDYN>\n" +
				"            <CRADVICEREQDYN>Y</CRADVICEREQDYN>\n" +
				"            <CHARGEDCUSTOMER>100001</CHARGEDCUSTOMER>\n" +
				"            <TOTRECCOMM>0</TOTRECCOMM>\n" +
				"            <TOTRECCOMMLCL>0</TOTRECCOMMLCL>\n" +
				"            <TOTRECCHG>0</TOTRECCHG>\n" +
				"            <TOTRECCHGLCL>0</TOTRECCHGLCL>\n" +
				"            <RATEFIXING>NO</RATEFIXING>\n" +
				"            <TOTRECCHGCRCCY>0</TOTRECCHGCRCCY>\n" +
				"            <TOTSNDCHGCRCCY>0</TOTSNDCHGCRCCY>\n" +
				"            <AUTHDATE>20170821</AUTHDATE>\n" +
				"            <ROUNDTYPE>NATURAL</ROUNDTYPE>\n" +
				"            <gSTMTNOS>\n" +
				"               <STMTNOS>184280369364626.00</STMTNOS>\n" +
				"               <STMTNOS>1-2</STMTNOS>\n" +
				"               <STMTNOS>ET0010051</STMTNOS>\n" +
				"               <STMTNOS>184280369364626.01</STMTNOS>\n" +
				"               <STMTNOS>1-2</STMTNOS>\n" +
				"               <STMTNOS>ET0010009</STMTNOS>\n" +
				"               <STMTNOS>184280369364626.02</STMTNOS>\n" +
				"               <STMTNOS>1-2</STMTNOS>\n" +
				"            </gSTMTNOS>\n" +
				"            <CURRNO>1</CURRNO>\n" +
				"            <gINPUTTER>\n" +
				"               <INPUTTER>3693_INPUTTER__OFS_TWS</INPUTTER>\n" +
				"            </gINPUTTER>\n" +
				"            <gDATETIME>\n" +
				"               <DATETIME>1806141757</DATETIME>\n" +
				"            </gDATETIME>\n" +
				"            <AUTHORISER>3693_INPUTTER_OFS_TWS</AUTHORISER>\n" +
				"            <COCODE>ET0010001</COCODE>\n" +
				"            <DEPTCODE>1</DEPTCODE>\n" +
				"         </FUNDSTRANSFERType>\n" +
				"      </ns2:WALLETACCTOACCResponse>\n" +
				"   </S:Body>\n" +
				"</S:Envelope>";





		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		InputSource is;
		try {
			builder = factory.newDocumentBuilder();
			is = new InputSource(new StringReader(xml));
			Document doc = builder.parse(is);
			NodeList list = doc.getElementsByTagName("successIndicator");
			System.out.println(list.item(0).getTextContent());
		} catch (ParserConfigurationException e) {
		} catch (SAXException e) {
		} catch (IOException e) {
		}
	}


}
