package com.dc.Integration.boa.exception;

import com.dc.Integration.boa.pojo.MessageBody;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerAdvice {


    @ExceptionHandler(ServiceFaultException.class)
    public ResponseEntity<MessageBody> handleException(ServiceFaultException exception) {
        MessageBody messageBody = MessageBody.getInstance();
        messageBody.setStatus(exception.getHttpStatus().value()+"");
        messageBody.setText(exception.getMessage());
        return new ResponseEntity<>(messageBody, exception.getHttpStatus());
    }
}
