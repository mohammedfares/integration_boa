package com.dc.Integration.boa.pojo;

import java.time.LocalDateTime;

public class WalletRegestreationRq {


    private  String activityName;

    private String assignReason;

    private LocalDateTime dueDate;

    public String extProcess;

    private Long extProcessID;

    private String gtsControl;

    private Long messageId;

    private  Long noOfAuth;

    private String owner;

    private  String replace;

    private LocalDateTime startDate;

    private String user;

    private Long boaAccountNo;

    private  String walletNumber;

    private String accountType;

    private String mobileNumber;


    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getAssignReason() {
        return assignReason;
    }

    public void setAssignReason(String assignReason) {
        this.assignReason = assignReason;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public String getExtProcess() {
        return extProcess;
    }

    public void setExtProcess(String extProcess) {
        this.extProcess = extProcess;
    }

    public Long getExtProcessID() {
        return extProcessID;
    }

    public void setExtProcessID(Long extProcessID) {
        this.extProcessID = extProcessID;
    }

    public String getGtsControl() {
        return gtsControl;
    }

    public void setGtsControl(String gtsControl) {
        this.gtsControl = gtsControl;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getNoOfAuth() {
        return noOfAuth;
    }

    public void setNoOfAuth(Long noOfAuth) {
        this.noOfAuth = noOfAuth;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getReplace() {
        return replace;
    }

    public void setReplace(String replace) {
        this.replace = replace;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getBoaAccountNo() {
        return boaAccountNo;
    }

    public void setBoaAccountNo(Long boaAccountNo) {
        this.boaAccountNo = boaAccountNo;
    }

    public String getWalletNumber() {
        return walletNumber;
    }

    public void setWalletNumber(String walletNumber) {
        this.walletNumber = walletNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }
}
