package com.dc.Integration.boa.pojo;

public class OTCInquireRs {

    private String amount;

    private String senderFullName;

    private String senderIdentityNo;

    private String senderMobileNo;

    private String receiverFullName;

    private String receiverIdentityNo;

    private String receiverMobileNo;

    private String senderBranchCode;

    private String receiverBranchCode;

    private String status;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSenderFullName() {
        return senderFullName;
    }

    public void setSenderFullName(String senderFullName) {
        this.senderFullName = senderFullName;
    }

    public String getSenderIdentityNo() {
        return senderIdentityNo;
    }

    public void setSenderIdentityNo(String senderIdentityNo) {
        this.senderIdentityNo = senderIdentityNo;
    }

    public String getSenderMobileNo() {
        return senderMobileNo;
    }

    public void setSenderMobileNo(String senderMobileNo) {
        this.senderMobileNo = senderMobileNo;
    }

    public String getReceiverFullName() {
        return receiverFullName;
    }

    public void setReceiverFullName(String receiverFullName) {
        this.receiverFullName = receiverFullName;
    }

    public String getReceiverIdentityNo() {
        return receiverIdentityNo;
    }

    public void setReceiverIdentityNo(String receiverIdentityNo) {
        this.receiverIdentityNo = receiverIdentityNo;
    }

    public String getReceiverMobileNo() {
        return receiverMobileNo;
    }

    public void setReceiverMobileNo(String receiverMobileNo) {
        this.receiverMobileNo = receiverMobileNo;
    }

    public String getSenderBranchCode() {
        return senderBranchCode;
    }

    public void setSenderBranchCode(String senderBranchCode) {
        this.senderBranchCode = senderBranchCode;
    }

    public String getReceiverBranchCode() {
        return receiverBranchCode;
    }

    public void setReceiverBranchCode(String receiverBranchCode) {
        this.receiverBranchCode = receiverBranchCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
