package com.dc.Integration.boa.pojo;

public class OTCReceiveRq {

    private String internalReference;
    private Beneficiary receiverBeneficiary;

    public String getInternalReference() {
        return internalReference;
    }

    public void setInternalReference(String internalReference) {
        this.internalReference = internalReference;
    }

    public Beneficiary getReceiverBeneficiary() {
        return receiverBeneficiary;
    }

    public void setReceiverBeneficiary(Beneficiary receiverBeneficiary) {
        this.receiverBeneficiary = receiverBeneficiary;
    }
}
