package com.dc.Integration.boa.pojo;

public class BankDataTransferRq {

    private String transactionId;

    private String transactionType;

    private String debitAccNo;

    private String debitCurrency;

    private Double debitAmount;

    private String debitTheIRREF;

    public String creditTheIRREF;

    private String creditAccNo;

    private String benCustomer;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDebitAccNo() {
        return debitAccNo;
    }

    public void setDebitAccNo(String debitAccNo) {
        this.debitAccNo = debitAccNo;
    }

    public String getDebitCurrency() {
        return debitCurrency;
    }

    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    public Double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getDebitTheIRREF() {
        return debitTheIRREF;
    }

    public void setDebitTheIRREF(String debitTheIRREF) {
        this.debitTheIRREF = debitTheIRREF;
    }

    public String getCreditTheIRREF() {
        return creditTheIRREF;
    }

    public void setCreditTheIRREF(String creditTheIRREF) {
        this.creditTheIRREF = creditTheIRREF;
    }

    public String getCreditAccNo() {
        return creditAccNo;
    }

    public void setCreditAccNo(String creditAccNo) {
        this.creditAccNo = creditAccNo;
    }

    public String getBenCustomer() {
        return benCustomer;
    }

    public void setBenCustomer(String benCustomer) {
        this.benCustomer = benCustomer;
    }
}
