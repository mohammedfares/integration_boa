package com.dc.Integration.boa.pojo;

public class BeneficiaryRq {


    private String transactionType;

    private String debitAccNo;

    private String debitCurrency;

    private double debitAmount;

    private String debitTheIRREF;

    private String creditTheIRREF;

    private Long creditAccNo;

    private String benCustomer;


    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getDebitAccNo() {
        return debitAccNo;
    }

    public void setDebitAccNo(String debitAccNo) {
        this.debitAccNo = debitAccNo;
    }

    public String getDebitCurrency() {
        return debitCurrency;
    }

    public void setDebitCurrency(String debitCurrency) {
        this.debitCurrency = debitCurrency;
    }

    public double getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(double debitAmount) {
        this.debitAmount = debitAmount;
    }

    public String getDebitTheIRREF() {
        return debitTheIRREF;
    }

    public void setDebitTheIRREF(String debitTheIRREF) {
        this.debitTheIRREF = debitTheIRREF;
    }

    public String getCreditTheIRREF() {
        return creditTheIRREF;
    }

    public void setCreditTheIRREF(String creditTheIRREF) {
        this.creditTheIRREF = creditTheIRREF;
    }

    public Long getCreditAccNo() {
        return creditAccNo;
    }

    public void setCreditAccNo(Long creditAccNo) {
        this.creditAccNo = creditAccNo;
    }

    public String getBenCustomer() {
        return benCustomer;
    }

    public void setBenCustomer(String benCustomer) {
        this.benCustomer = benCustomer;
    }
}
