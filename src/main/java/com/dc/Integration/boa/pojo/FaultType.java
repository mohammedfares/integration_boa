package com.dc.Integration.boa.pojo;

public class FaultType {

    protected String code;
    protected String description;
    protected String reason;



    public String getCode() {
        return code;
    }

    public void setCode(String value) {
        this.code = value;
    }





    public String getDescription() {
        return description;
    }


    public void setDescription(String value) {
        this.description = value;
    }


    public String getReason() {
        return reason;
    }

    public void setReason(String value) {
        this.reason = value;
    }



}
