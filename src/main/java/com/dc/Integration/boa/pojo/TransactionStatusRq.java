package com.dc.Integration.boa.pojo;

public class TransactionStatusRq {

    private String criteriaValue;

    private String columnName;

    private String operand;

    private String transactionId;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCriteriaValue() {
        return criteriaValue;
    }

    public void setCriteriaValue(String criteriaValue) {
        this.criteriaValue = criteriaValue;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getOperand() {
        return operand;
    }

    public void setOperand(String operand) {
        this.operand = operand;
    }

    @Override
    public String toString() {
        return "TransactionStatusRq{" +
                "criteriaValue='" + criteriaValue + '\'' +
                ", columnName='" + columnName + '\'' +
                ", operand='" + operand + '\'' +
                '}';
    }
}
