package com.dc.Integration.boa.pojo;

import java.time.LocalDateTime;

public class CashoutRs {

    private String status;

    private String t24Reference;

    private String liyuPayReference;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getT24Reference() {
        return t24Reference;
    }

    public void setT24Reference(String t24Reference) {
        this.t24Reference = t24Reference;
    }

    public String getLiyuPayReference() {
        return liyuPayReference;
    }

    public void setLiyuPayReference(String liyuPayReference) {
        this.liyuPayReference = liyuPayReference;
    }
}
