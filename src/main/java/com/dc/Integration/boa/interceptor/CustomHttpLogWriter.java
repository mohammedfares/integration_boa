package com.dc.Integration.boa.interceptor;

import com.dc.Integration.boa.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.zalando.logbook.Correlation;
import org.zalando.logbook.HttpLogWriter;
import org.zalando.logbook.Precorrelation;
import org.zalando.logbook.RawHttpRequest;

import java.io.IOException;

@Component
public class CustomHttpLogWriter implements HttpLogWriter {

    @Autowired
    private LogService logService;

    @Override
    public boolean isActive(RawHttpRequest request) throws IOException {
        return true;
    }

    @Override
    public void writeRequest(Precorrelation<String> precorrelation) throws IOException {
    }

    @Override
    public void writeResponse(Correlation<String, String> correlation) throws IOException {
        logService.logHttp(correlation);
    }
}
