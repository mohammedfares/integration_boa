package com.dc.Integration.boa.config;

import com.dc.Integration.boa.interceptor.CustomHttpLogWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.logbook.Logbook;

@Configuration
public class AppConfig {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private CustomHttpLogWriter customHttpLogWriter;

    @Bean
    public Logbook logbook(){
        Logbook logbook = Logbook.builder()
                .writer(customHttpLogWriter)
                .build();
        return logbook;
    }

}